
// db.rooms.find();

// NUMBER 3 - INSERT ONE METHOD
db.rooms.insertOne({
        "name": "Single",
        "accomodates": 2,
        "price": 1000,
        "description": "A simple room with all the basic necessities",
        "rooms_available": 10,
        "isAvailable": "false"    
    });

// NUMBER 4 - INSERT MANY METHOD
db.rooms.insertMany([{
        "name": "Double",
        "accomodates": 3,
        "price": 2000,
        "description": "A room fit for a small family goin on a vacation",
        "rooms_available": 5,
        "isAvailable": "false"      
},
{
        "name": "test",
        "accomodates": 4,
        "price": 4000,
        "description": "A room with queen sized bed perfect for a simple getaway",
        "rooms_available": 15,
        "isAvailable": "false"    
}   
]);

// NUMBER 5 - FIND METHOD
db.rooms.find({"name": "Double"});

// NUMBER 6 - UPDATE ONE METHOD
db.rooms.updateOne(
        { "name": "test" },
        {
            $set: {
            "name": "Queen Room",
             "rooms_available": 0,
                }
         }
);
       
// NUMBER 7 - DELETE MANY METHOD
db.rooms.deleteMany(
     {"rooms_available": 0}
     
);

// DONE